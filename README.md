# IMPORTANT: This project has moved to Codeberg: https://codeberg.org/fswb/kindmind

<a href="https://f-droid.org/packages/com.sunyata.kindmind/"><img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height=90></a><a href='https://play.google.com/store/apps/details?id=com.sunyata.kindmind'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height="90"/></a>

# KindMind

KindMind is a customizable mindfulness and self-compassion app for Android which brings awareness to painful feelings and the unmet needs causing these feelings. KindMind can be customized to suggest kind actions that can be taken after the feelings and need have been identified, and these actions can optionally be connected to various media. The app contains a sorting algorithm which puts the most relevant list items highest, taking into account relationships between list items that the user has chosen together so that the sorting improves as the app is used


